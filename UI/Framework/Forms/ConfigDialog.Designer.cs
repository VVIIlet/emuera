﻿namespace MinorShift.Emuera.Forms
{
	partial class ConfigDialog
	{
		/// <summary>
		/// 必要なデザイナ変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

        #region Windows フォーム デザイナで生成されたコード

        /// <summary>
        /// デザイナ サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディタで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigDialog));
            buttonSave = new System.Windows.Forms.Button();
            buttonCancel = new System.Windows.Forms.Button();
            buttonReboot = new System.Windows.Forms.Button();
            tabControl = new System.Windows.Forms.TabControl();
            tabEnvironment = new System.Windows.Forms.TabPage();
            comboBox6 = new System.Windows.Forms.ComboBox();
            checkBox24 = new System.Windows.Forms.CheckBox();
            textBox2 = new System.Windows.Forms.TextBox();
            label23 = new System.Windows.Forms.Label();
            button4 = new System.Windows.Forms.Button();
            textBox1 = new System.Windows.Forms.TextBox();
            label22 = new System.Windows.Forms.Label();
            label20 = new System.Windows.Forms.Label();
            label17 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            numericUpDown11 = new System.Windows.Forms.NumericUpDown();
            checkBox18 = new System.Windows.Forms.CheckBox();
            numericUpDown10 = new System.Windows.Forms.NumericUpDown();
            numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            checkBox7 = new System.Windows.Forms.CheckBox();
            checkBox6 = new System.Windows.Forms.CheckBox();
            checkBox5 = new System.Windows.Forms.CheckBox();
            checkBox4 = new System.Windows.Forms.CheckBox();
            checkBox3 = new System.Windows.Forms.CheckBox();
            tabPageView = new System.Windows.Forms.TabPage();
            _useButtonFocusColor = new System.Windows.Forms.CheckBox();
            checkBox14 = new System.Windows.Forms.CheckBox();
            label18 = new System.Windows.Forms.Label();
            comboBoxTextDrawingMode = new System.Windows.Forms.ComboBox();
            label1 = new System.Windows.Forms.Label();
            numericUpDown9 = new System.Windows.Forms.NumericUpDown();
            label5 = new System.Windows.Forms.Label();
            numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            label9 = new System.Windows.Forms.Label();
            numericUpDown7 = new System.Windows.Forms.NumericUpDown();
            tabPageWindow = new System.Windows.Forms.TabPage();
            checkBox21 = new System.Windows.Forms.CheckBox();
            ScrollRange = new System.Windows.Forms.Label();
            numericUpDown8 = new System.Windows.Forms.NumericUpDown();
            checkBox17 = new System.Windows.Forms.CheckBox();
            button3 = new System.Windows.Forms.Button();
            label10 = new System.Windows.Forms.Label();
            label19 = new System.Windows.Forms.Label();
            numericUpDownPosY = new System.Windows.Forms.NumericUpDown();
            numericUpDownPosX = new System.Windows.Forms.NumericUpDown();
            button1 = new System.Windows.Forms.Button();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            checkBox8 = new System.Windows.Forms.CheckBox();
            tabPageFont = new System.Windows.Forms.TabPage();
            flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            colorBoxBG = new ColorBox();
            colorBoxFG = new ColorBox();
            colorBoxSelecting = new ColorBox();
            colorBoxBacklog = new ColorBox();
            flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            comboBox2 = new System.Windows.Forms.ComboBox();
            label4 = new System.Windows.Forms.Label();
            numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            numericUpDown6 = new System.Windows.Forms.NumericUpDown();
            tabPageSystem = new System.Windows.Forms.TabPage();
            comboBox1 = new System.Windows.Forms.ComboBox();
            label11 = new System.Windows.Forms.Label();
            checkBoxSystemFullSpace = new System.Windows.Forms.CheckBox();
            checkBox22 = new System.Windows.Forms.CheckBox();
            label21 = new System.Windows.Forms.Label();
            checkBox20 = new System.Windows.Forms.CheckBox();
            checkBox19 = new System.Windows.Forms.CheckBox();
            checkBox16 = new System.Windows.Forms.CheckBox();
            checkBox15 = new System.Windows.Forms.CheckBox();
            checkBox1 = new System.Windows.Forms.CheckBox();
            checkBox10 = new System.Windows.Forms.CheckBox();
            checkBox2 = new System.Windows.Forms.CheckBox();
            tabPageSystem2 = new System.Windows.Forms.TabPage();
            label24 = new System.Windows.Forms.Label();
            checkBox29 = new System.Windows.Forms.CheckBox();
            checkBox26 = new System.Windows.Forms.CheckBox();
            checkBox27 = new System.Windows.Forms.CheckBox();
            checkBoxSystemTripleSymbol = new System.Windows.Forms.CheckBox();
            _useScopedVariableInstruction = new System.Windows.Forms.CheckBox();
            _useNewRandom = new System.Windows.Forms.CheckBox();
            tabPageCompati = new System.Windows.Forms.TabPage();
            checkBox9 = new System.Windows.Forms.CheckBox();
            checkBoxCompatiSP = new System.Windows.Forms.CheckBox();
            checkBox28 = new System.Windows.Forms.CheckBox();
            checkBox25 = new System.Windows.Forms.CheckBox();
            checkBox12 = new System.Windows.Forms.CheckBox();
            checkBoxFuncNoIgnoreCase = new System.Windows.Forms.CheckBox();
            button8 = new System.Windows.Forms.Button();
            button7 = new System.Windows.Forms.Button();
            checkBoxCompatiLinefeedAs1739 = new System.Windows.Forms.CheckBox();
            checkBoxCompatiRAND = new System.Windows.Forms.CheckBox();
            label30 = new System.Windows.Forms.Label();
            checkBoxCompatiCALLNAME = new System.Windows.Forms.CheckBox();
            checkBoxCompatiErrorLine = new System.Windows.Forms.CheckBox();
            tabPageDebug = new System.Windows.Forms.TabPage();
            button6 = new System.Windows.Forms.Button();
            button5 = new System.Windows.Forms.Button();
            checkBox23 = new System.Windows.Forms.CheckBox();
            label15 = new System.Windows.Forms.Label();
            comboBox5 = new System.Windows.Forms.ComboBox();
            label14 = new System.Windows.Forms.Label();
            comboBox4 = new System.Windows.Forms.ComboBox();
            label13 = new System.Windows.Forms.Label();
            comboBox3 = new System.Windows.Forms.ComboBox();
            label12 = new System.Windows.Forms.Label();
            comboBoxReduceArgumentOnLoad = new System.Windows.Forms.ComboBox();
            checkBox11 = new System.Windows.Forms.CheckBox();
            checkBox13 = new System.Windows.Forms.CheckBox();
            label16 = new System.Windows.Forms.Label();
            openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            toolTip1 = new System.Windows.Forms.ToolTip(components);
            fontDialog1 = new System.Windows.Forms.FontDialog();
            tabControl.SuspendLayout();
            tabEnvironment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)numericUpDown11).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown10).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown4).BeginInit();
            tabPageView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)numericUpDown9).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown7).BeginInit();
            tabPageWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)numericUpDown8).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDownPosY).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDownPosX).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown2).BeginInit();
            tabPageFont.SuspendLayout();
            flowLayoutPanel1.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)numericUpDown5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown6).BeginInit();
            tabPageSystem.SuspendLayout();
            tabPageSystem2.SuspendLayout();
            tabPageCompati.SuspendLayout();
            tabPageDebug.SuspendLayout();
            SuspendLayout();
            // 
            // buttonSave
            // 
            resources.ApplyResources(buttonSave, "buttonSave");
            buttonSave.Name = "buttonSave";
            buttonSave.UseVisualStyleBackColor = true;
            buttonSave.Click += buttonSave_Click;
            // 
            // buttonCancel
            // 
            resources.ApplyResources(buttonCancel, "buttonCancel");
            buttonCancel.Name = "buttonCancel";
            buttonCancel.UseVisualStyleBackColor = true;
            buttonCancel.Click += buttonCancel_Click;
            // 
            // buttonReboot
            // 
            resources.ApplyResources(buttonReboot, "buttonReboot");
            buttonReboot.Name = "buttonReboot";
            buttonReboot.UseVisualStyleBackColor = true;
            buttonReboot.Click += buttonReboot_Click;
            // 
            // tabControl
            // 
            tabControl.Controls.Add(tabEnvironment);
            tabControl.Controls.Add(tabPageView);
            tabControl.Controls.Add(tabPageWindow);
            tabControl.Controls.Add(tabPageFont);
            tabControl.Controls.Add(tabPageSystem);
            tabControl.Controls.Add(tabPageSystem2);
            tabControl.Controls.Add(tabPageCompati);
            tabControl.Controls.Add(tabPageDebug);
            resources.ApplyResources(tabControl, "tabControl");
            tabControl.Multiline = true;
            tabControl.Name = "tabControl";
            tabControl.SelectedIndex = 0;
            // 
            // tabEnvironment
            // 
            tabEnvironment.Controls.Add(comboBox6);
            tabEnvironment.Controls.Add(checkBox24);
            tabEnvironment.Controls.Add(textBox2);
            tabEnvironment.Controls.Add(label23);
            tabEnvironment.Controls.Add(button4);
            tabEnvironment.Controls.Add(textBox1);
            tabEnvironment.Controls.Add(label22);
            tabEnvironment.Controls.Add(label20);
            tabEnvironment.Controls.Add(label17);
            tabEnvironment.Controls.Add(label6);
            tabEnvironment.Controls.Add(numericUpDown11);
            tabEnvironment.Controls.Add(checkBox18);
            tabEnvironment.Controls.Add(numericUpDown10);
            tabEnvironment.Controls.Add(numericUpDown4);
            tabEnvironment.Controls.Add(checkBox7);
            tabEnvironment.Controls.Add(checkBox6);
            tabEnvironment.Controls.Add(checkBox5);
            tabEnvironment.Controls.Add(checkBox4);
            tabEnvironment.Controls.Add(checkBox3);
            resources.ApplyResources(tabEnvironment, "tabEnvironment");
            tabEnvironment.Name = "tabEnvironment";
            tabEnvironment.UseVisualStyleBackColor = true;
            // 
            // comboBox6
            // 
            comboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            comboBox6.FormattingEnabled = true;
            comboBox6.Items.AddRange(new object[] { resources.GetString("comboBox6.Items"), resources.GetString("comboBox6.Items1"), resources.GetString("comboBox6.Items2"), resources.GetString("comboBox6.Items3") });
            resources.ApplyResources(comboBox6, "comboBox6");
            comboBox6.Name = "comboBox6";
            comboBox6.SelectedIndexChanged += comboBox6_SelectedIndexChanged;
            // 
            // checkBox24
            // 
            resources.ApplyResources(checkBox24, "checkBox24");
            checkBox24.Name = "checkBox24";
            checkBox24.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            resources.ApplyResources(textBox2, "textBox2");
            textBox2.Name = "textBox2";
            // 
            // label23
            // 
            resources.ApplyResources(label23, "label23");
            label23.Name = "label23";
            // 
            // button4
            // 
            resources.ApplyResources(button4, "button4");
            button4.Name = "button4";
            button4.UseVisualStyleBackColor = true;
            button4.Click += button4_Click;
            // 
            // textBox1
            // 
            resources.ApplyResources(textBox1, "textBox1");
            textBox1.Name = "textBox1";
            // 
            // label22
            // 
            resources.ApplyResources(label22, "label22");
            label22.Name = "label22";
            // 
            // label20
            // 
            resources.ApplyResources(label20, "label20");
            label20.Name = "label20";
            // 
            // label17
            // 
            resources.ApplyResources(label17, "label17");
            label17.Name = "label17";
            // 
            // label6
            // 
            resources.ApplyResources(label6, "label6");
            label6.Name = "label6";
            // 
            // numericUpDown11
            // 
            resources.ApplyResources(numericUpDown11, "numericUpDown11");
            numericUpDown11.Name = "numericUpDown11";
            // 
            // checkBox18
            // 
            resources.ApplyResources(checkBox18, "checkBox18");
            checkBox18.Name = "checkBox18";
            checkBox18.UseVisualStyleBackColor = true;
            // 
            // numericUpDown10
            // 
            resources.ApplyResources(numericUpDown10, "numericUpDown10");
            numericUpDown10.Name = "numericUpDown10";
            // 
            // numericUpDown4
            // 
            resources.ApplyResources(numericUpDown4, "numericUpDown4");
            numericUpDown4.Name = "numericUpDown4";
            // 
            // checkBox7
            // 
            resources.ApplyResources(checkBox7, "checkBox7");
            checkBox7.Name = "checkBox7";
            checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            resources.ApplyResources(checkBox6, "checkBox6");
            checkBox6.Name = "checkBox6";
            checkBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            resources.ApplyResources(checkBox5, "checkBox5");
            checkBox5.Name = "checkBox5";
            checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            resources.ApplyResources(checkBox4, "checkBox4");
            checkBox4.Name = "checkBox4";
            checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            resources.ApplyResources(checkBox3, "checkBox3");
            checkBox3.Name = "checkBox3";
            checkBox3.UseVisualStyleBackColor = true;
            // 
            // tabPageView
            // 
            tabPageView.Controls.Add(_useButtonFocusColor);
            tabPageView.Controls.Add(checkBox14);
            tabPageView.Controls.Add(label18);
            tabPageView.Controls.Add(comboBoxTextDrawingMode);
            tabPageView.Controls.Add(label1);
            tabPageView.Controls.Add(numericUpDown9);
            tabPageView.Controls.Add(label5);
            tabPageView.Controls.Add(numericUpDown1);
            tabPageView.Controls.Add(label9);
            tabPageView.Controls.Add(numericUpDown7);
            resources.ApplyResources(tabPageView, "tabPageView");
            tabPageView.Name = "tabPageView";
            tabPageView.UseVisualStyleBackColor = true;
            // 
            // _useButtonFocusColor
            // 
            resources.ApplyResources(_useButtonFocusColor, "_useButtonFocusColor");
            _useButtonFocusColor.Name = "_useButtonFocusColor";
            _useButtonFocusColor.UseVisualStyleBackColor = true;
            _useButtonFocusColor.CheckedChanged += UseButtonFocusColor_CheckedChanged;
            // 
            // checkBox14
            // 
            resources.ApplyResources(checkBox14, "checkBox14");
            checkBox14.Name = "checkBox14";
            checkBox14.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            resources.ApplyResources(label18, "label18");
            label18.Name = "label18";
            // 
            // comboBoxTextDrawingMode
            // 
            comboBoxTextDrawingMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            comboBoxTextDrawingMode.FormattingEnabled = true;
            comboBoxTextDrawingMode.Items.AddRange(new object[] { resources.GetString("comboBoxTextDrawingMode.Items"), resources.GetString("comboBoxTextDrawingMode.Items1") });
            resources.ApplyResources(comboBoxTextDrawingMode, "comboBoxTextDrawingMode");
            comboBoxTextDrawingMode.Name = "comboBoxTextDrawingMode";
            // 
            // label1
            // 
            resources.ApplyResources(label1, "label1");
            label1.Name = "label1";
            // 
            // numericUpDown9
            // 
            resources.ApplyResources(numericUpDown9, "numericUpDown9");
            numericUpDown9.Name = "numericUpDown9";
            // 
            // label5
            // 
            resources.ApplyResources(label5, "label5");
            label5.Name = "label5";
            // 
            // numericUpDown1
            // 
            resources.ApplyResources(numericUpDown1, "numericUpDown1");
            numericUpDown1.Name = "numericUpDown1";
            // 
            // label9
            // 
            resources.ApplyResources(label9, "label9");
            label9.Name = "label9";
            // 
            // numericUpDown7
            // 
            resources.ApplyResources(numericUpDown7, "numericUpDown7");
            numericUpDown7.Name = "numericUpDown7";
            // 
            // tabPageWindow
            // 
            tabPageWindow.Controls.Add(checkBox21);
            tabPageWindow.Controls.Add(ScrollRange);
            tabPageWindow.Controls.Add(numericUpDown8);
            tabPageWindow.Controls.Add(checkBox17);
            tabPageWindow.Controls.Add(button3);
            tabPageWindow.Controls.Add(label10);
            tabPageWindow.Controls.Add(label19);
            tabPageWindow.Controls.Add(numericUpDownPosY);
            tabPageWindow.Controls.Add(numericUpDownPosX);
            tabPageWindow.Controls.Add(button1);
            tabPageWindow.Controls.Add(label3);
            tabPageWindow.Controls.Add(label2);
            tabPageWindow.Controls.Add(numericUpDown3);
            tabPageWindow.Controls.Add(numericUpDown2);
            tabPageWindow.Controls.Add(checkBox8);
            resources.ApplyResources(tabPageWindow, "tabPageWindow");
            tabPageWindow.Name = "tabPageWindow";
            tabPageWindow.UseVisualStyleBackColor = true;
            // 
            // checkBox21
            // 
            resources.ApplyResources(checkBox21, "checkBox21");
            checkBox21.Name = "checkBox21";
            checkBox21.UseVisualStyleBackColor = true;
            // 
            // ScrollRange
            // 
            resources.ApplyResources(ScrollRange, "ScrollRange");
            ScrollRange.Name = "ScrollRange";
            // 
            // numericUpDown8
            // 
            resources.ApplyResources(numericUpDown8, "numericUpDown8");
            numericUpDown8.Name = "numericUpDown8";
            // 
            // checkBox17
            // 
            resources.ApplyResources(checkBox17, "checkBox17");
            checkBox17.Name = "checkBox17";
            checkBox17.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            resources.ApplyResources(button3, "button3");
            button3.Name = "button3";
            button3.UseVisualStyleBackColor = true;
            button3.Click += button3_Click;
            // 
            // label10
            // 
            resources.ApplyResources(label10, "label10");
            label10.Name = "label10";
            // 
            // label19
            // 
            resources.ApplyResources(label19, "label19");
            label19.Name = "label19";
            // 
            // numericUpDownPosY
            // 
            resources.ApplyResources(numericUpDownPosY, "numericUpDownPosY");
            numericUpDownPosY.Name = "numericUpDownPosY";
            // 
            // numericUpDownPosX
            // 
            resources.ApplyResources(numericUpDownPosX, "numericUpDownPosX");
            numericUpDownPosX.Name = "numericUpDownPosX";
            // 
            // button1
            // 
            resources.ApplyResources(button1, "button1");
            button1.Name = "button1";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // label3
            // 
            resources.ApplyResources(label3, "label3");
            label3.Name = "label3";
            // 
            // label2
            // 
            resources.ApplyResources(label2, "label2");
            label2.Name = "label2";
            // 
            // numericUpDown3
            // 
            resources.ApplyResources(numericUpDown3, "numericUpDown3");
            numericUpDown3.Name = "numericUpDown3";
            // 
            // numericUpDown2
            // 
            resources.ApplyResources(numericUpDown2, "numericUpDown2");
            numericUpDown2.Name = "numericUpDown2";
            // 
            // checkBox8
            // 
            resources.ApplyResources(checkBox8, "checkBox8");
            checkBox8.Name = "checkBox8";
            checkBox8.UseVisualStyleBackColor = true;
            // 
            // tabPageFont
            // 
            tabPageFont.Controls.Add(flowLayoutPanel1);
            resources.ApplyResources(tabPageFont, "tabPageFont");
            tabPageFont.Name = "tabPageFont";
            tabPageFont.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            flowLayoutPanel1.Controls.Add(colorBoxBG);
            flowLayoutPanel1.Controls.Add(colorBoxFG);
            flowLayoutPanel1.Controls.Add(colorBoxSelecting);
            flowLayoutPanel1.Controls.Add(colorBoxBacklog);
            flowLayoutPanel1.Controls.Add(flowLayoutPanel2);
            flowLayoutPanel1.Controls.Add(flowLayoutPanel3);
            flowLayoutPanel1.Controls.Add(flowLayoutPanel4);
            flowLayoutPanel1.Controls.Add(tableLayoutPanel1);
            resources.ApplyResources(flowLayoutPanel1, "flowLayoutPanel1");
            flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // colorBoxBG
            // 
            resources.ApplyResources(colorBoxBG, "colorBoxBG");
            colorBoxBG.LabelText = "背景色";
            colorBoxBG.Name = "colorBoxBG";
            colorBoxBG.SelectingColor = System.Drawing.Color.Transparent;
            // 
            // colorBoxFG
            // 
            resources.ApplyResources(colorBoxFG, "colorBoxFG");
            colorBoxFG.LabelText = "文字色";
            colorBoxFG.Name = "colorBoxFG";
            colorBoxFG.SelectingColor = System.Drawing.Color.Transparent;
            // 
            // colorBoxSelecting
            // 
            resources.ApplyResources(colorBoxSelecting, "colorBoxSelecting");
            colorBoxSelecting.LabelText = "選択中文字色";
            colorBoxSelecting.Name = "colorBoxSelecting";
            colorBoxSelecting.SelectingColor = System.Drawing.Color.Transparent;
            // 
            // colorBoxBacklog
            // 
            resources.ApplyResources(colorBoxBacklog, "colorBoxBacklog");
            colorBoxBacklog.LabelText = "履歴文字色";
            colorBoxBacklog.Name = "colorBoxBacklog";
            colorBoxBacklog.SelectingColor = System.Drawing.Color.Transparent;
            // 
            // flowLayoutPanel2
            // 
            resources.ApplyResources(flowLayoutPanel2, "flowLayoutPanel2");
            flowLayoutPanel2.Name = "flowLayoutPanel2";
            // 
            // flowLayoutPanel3
            // 
            resources.ApplyResources(flowLayoutPanel3, "flowLayoutPanel3");
            flowLayoutPanel3.Name = "flowLayoutPanel3";
            // 
            // flowLayoutPanel4
            // 
            resources.ApplyResources(flowLayoutPanel4, "flowLayoutPanel4");
            flowLayoutPanel4.Name = "flowLayoutPanel4";
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(tableLayoutPanel1, "tableLayoutPanel1");
            tableLayoutPanel1.Controls.Add(label7, 0, 2);
            tableLayoutPanel1.Controls.Add(label8, 0, 1);
            tableLayoutPanel1.Controls.Add(comboBox2, 1, 0);
            tableLayoutPanel1.Controls.Add(label4, 0, 0);
            tableLayoutPanel1.Controls.Add(numericUpDown5, 1, 1);
            tableLayoutPanel1.Controls.Add(numericUpDown6, 1, 2);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // label7
            // 
            resources.ApplyResources(label7, "label7");
            label7.Name = "label7";
            // 
            // label8
            // 
            resources.ApplyResources(label8, "label8");
            label8.Name = "label8";
            // 
            // comboBox2
            // 
            resources.ApplyResources(comboBox2, "comboBox2");
            comboBox2.Name = "comboBox2";
            // 
            // label4
            // 
            resources.ApplyResources(label4, "label4");
            label4.Name = "label4";
            // 
            // numericUpDown5
            // 
            resources.ApplyResources(numericUpDown5, "numericUpDown5");
            numericUpDown5.Name = "numericUpDown5";
            numericUpDown5.ValueChanged += numericUpDown5_ValueChanged;
            // 
            // numericUpDown6
            // 
            resources.ApplyResources(numericUpDown6, "numericUpDown6");
            numericUpDown6.Name = "numericUpDown6";
            // 
            // tabPageSystem
            // 
            tabPageSystem.Controls.Add(comboBox1);
            tabPageSystem.Controls.Add(label11);
            tabPageSystem.Controls.Add(checkBoxSystemFullSpace);
            tabPageSystem.Controls.Add(checkBox22);
            tabPageSystem.Controls.Add(label21);
            tabPageSystem.Controls.Add(checkBox20);
            tabPageSystem.Controls.Add(checkBox19);
            tabPageSystem.Controls.Add(checkBox16);
            tabPageSystem.Controls.Add(checkBox15);
            tabPageSystem.Controls.Add(checkBox1);
            tabPageSystem.Controls.Add(checkBox10);
            tabPageSystem.Controls.Add(checkBox2);
            resources.ApplyResources(tabPageSystem, "tabPageSystem");
            tabPageSystem.Name = "tabPageSystem";
            tabPageSystem.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            comboBox1.FormattingEnabled = true;
            comboBox1.Items.AddRange(new object[] { resources.GetString("comboBox1.Items"), resources.GetString("comboBox1.Items1"), resources.GetString("comboBox1.Items2"), resources.GetString("comboBox1.Items3") });
            resources.ApplyResources(comboBox1, "comboBox1");
            comboBox1.Name = "comboBox1";
            // 
            // label11
            // 
            resources.ApplyResources(label11, "label11");
            label11.Name = "label11";
            // 
            // checkBoxSystemFullSpace
            // 
            resources.ApplyResources(checkBoxSystemFullSpace, "checkBoxSystemFullSpace");
            checkBoxSystemFullSpace.Name = "checkBoxSystemFullSpace";
            checkBoxSystemFullSpace.UseVisualStyleBackColor = true;
            // 
            // checkBox22
            // 
            resources.ApplyResources(checkBox22, "checkBox22");
            checkBox22.Name = "checkBox22";
            checkBox22.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            resources.ApplyResources(label21, "label21");
            label21.Name = "label21";
            // 
            // checkBox20
            // 
            resources.ApplyResources(checkBox20, "checkBox20");
            checkBox20.Name = "checkBox20";
            checkBox20.UseVisualStyleBackColor = true;
            // 
            // checkBox19
            // 
            resources.ApplyResources(checkBox19, "checkBox19");
            checkBox19.Name = "checkBox19";
            checkBox19.UseVisualStyleBackColor = true;
            // 
            // checkBox16
            // 
            resources.ApplyResources(checkBox16, "checkBox16");
            checkBox16.Name = "checkBox16";
            checkBox16.UseVisualStyleBackColor = true;
            // 
            // checkBox15
            // 
            resources.ApplyResources(checkBox15, "checkBox15");
            checkBox15.Name = "checkBox15";
            checkBox15.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            resources.ApplyResources(checkBox1, "checkBox1");
            checkBox1.Name = "checkBox1";
            checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            resources.ApplyResources(checkBox10, "checkBox10");
            checkBox10.Name = "checkBox10";
            checkBox10.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            resources.ApplyResources(checkBox2, "checkBox2");
            checkBox2.Name = "checkBox2";
            checkBox2.UseVisualStyleBackColor = true;
            // 
            // tabPageSystem2
            // 
            tabPageSystem2.Controls.Add(label24);
            tabPageSystem2.Controls.Add(checkBox29);
            tabPageSystem2.Controls.Add(checkBox26);
            tabPageSystem2.Controls.Add(checkBox27);
            tabPageSystem2.Controls.Add(checkBoxSystemTripleSymbol);
            tabPageSystem2.Controls.Add(_useScopedVariableInstruction);
            tabPageSystem2.Controls.Add(_useNewRandom);
            resources.ApplyResources(tabPageSystem2, "tabPageSystem2");
            tabPageSystem2.Name = "tabPageSystem2";
            tabPageSystem2.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            resources.ApplyResources(label24, "label24");
            label24.Name = "label24";
            // 
            // checkBox29
            // 
            resources.ApplyResources(checkBox29, "checkBox29");
            checkBox29.Name = "checkBox29";
            checkBox29.UseVisualStyleBackColor = true;
            // 
            // checkBox26
            // 
            resources.ApplyResources(checkBox26, "checkBox26");
            checkBox26.Name = "checkBox26";
            checkBox26.UseVisualStyleBackColor = true;
            // 
            // checkBox27
            // 
            resources.ApplyResources(checkBox27, "checkBox27");
            checkBox27.Name = "checkBox27";
            checkBox27.UseVisualStyleBackColor = true;
            checkBox27.CheckedChanged += checkBox27_CheckedChanged;
            // 
            // checkBoxSystemTripleSymbol
            // 
            resources.ApplyResources(checkBoxSystemTripleSymbol, "checkBoxSystemTripleSymbol");
            checkBoxSystemTripleSymbol.Name = "checkBoxSystemTripleSymbol";
            checkBoxSystemTripleSymbol.UseVisualStyleBackColor = true;
            // 
            // _useScopedVariableInstruction
            // 
            resources.ApplyResources(_useScopedVariableInstruction, "_useScopedVariableInstruction");
            _useScopedVariableInstruction.Name = "_useScopedVariableInstruction";
            _useScopedVariableInstruction.UseVisualStyleBackColor = true;
            _useScopedVariableInstruction.CheckedChanged += _useVAR_CheckedChanged;
            // 
            // _useNewRandom
            // 
            resources.ApplyResources(_useNewRandom, "_useNewRandom");
            _useNewRandom.Name = "_useNewRandom";
            _useNewRandom.UseVisualStyleBackColor = true;
            _useNewRandom.CheckedChanged += IgnoreRandmizeSeed_CheckedChanged;
            // 
            // tabPageCompati
            // 
            tabPageCompati.Controls.Add(checkBox9);
            tabPageCompati.Controls.Add(checkBoxCompatiSP);
            tabPageCompati.Controls.Add(checkBox28);
            tabPageCompati.Controls.Add(checkBox25);
            tabPageCompati.Controls.Add(checkBox12);
            tabPageCompati.Controls.Add(checkBoxFuncNoIgnoreCase);
            tabPageCompati.Controls.Add(button8);
            tabPageCompati.Controls.Add(button7);
            tabPageCompati.Controls.Add(checkBoxCompatiLinefeedAs1739);
            tabPageCompati.Controls.Add(checkBoxCompatiRAND);
            tabPageCompati.Controls.Add(label30);
            tabPageCompati.Controls.Add(checkBoxCompatiCALLNAME);
            tabPageCompati.Controls.Add(checkBoxCompatiErrorLine);
            resources.ApplyResources(tabPageCompati, "tabPageCompati");
            tabPageCompati.Name = "tabPageCompati";
            tabPageCompati.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            resources.ApplyResources(checkBox9, "checkBox9");
            checkBox9.Name = "checkBox9";
            checkBox9.UseVisualStyleBackColor = true;
            // 
            // checkBoxCompatiSP
            // 
            resources.ApplyResources(checkBoxCompatiSP, "checkBoxCompatiSP");
            checkBoxCompatiSP.Name = "checkBoxCompatiSP";
            toolTip1.SetToolTip(checkBoxCompatiSP, resources.GetString("checkBoxCompatiSP.ToolTip"));
            checkBoxCompatiSP.UseVisualStyleBackColor = true;
            // 
            // checkBox28
            // 
            resources.ApplyResources(checkBox28, "checkBox28");
            checkBox28.Name = "checkBox28";
            toolTip1.SetToolTip(checkBox28, resources.GetString("checkBox28.ToolTip"));
            checkBox28.UseVisualStyleBackColor = true;
            // 
            // checkBox25
            // 
            resources.ApplyResources(checkBox25, "checkBox25");
            checkBox25.Name = "checkBox25";
            toolTip1.SetToolTip(checkBox25, resources.GetString("checkBox25.ToolTip"));
            checkBox25.UseVisualStyleBackColor = true;
            // 
            // checkBox12
            // 
            resources.ApplyResources(checkBox12, "checkBox12");
            checkBox12.Name = "checkBox12";
            toolTip1.SetToolTip(checkBox12, resources.GetString("checkBox12.ToolTip"));
            checkBox12.UseVisualStyleBackColor = true;
            // 
            // checkBoxFuncNoIgnoreCase
            // 
            resources.ApplyResources(checkBoxFuncNoIgnoreCase, "checkBoxFuncNoIgnoreCase");
            checkBoxFuncNoIgnoreCase.Name = "checkBoxFuncNoIgnoreCase";
            toolTip1.SetToolTip(checkBoxFuncNoIgnoreCase, resources.GetString("checkBoxFuncNoIgnoreCase.ToolTip"));
            checkBoxFuncNoIgnoreCase.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            resources.ApplyResources(button8, "button8");
            button8.Name = "button8";
            button8.UseVisualStyleBackColor = true;
            button8.Click += button8_Click;
            // 
            // button7
            // 
            resources.ApplyResources(button7, "button7");
            button7.Name = "button7";
            button7.UseVisualStyleBackColor = true;
            button7.Click += button7_Click;
            // 
            // checkBoxCompatiLinefeedAs1739
            // 
            resources.ApplyResources(checkBoxCompatiLinefeedAs1739, "checkBoxCompatiLinefeedAs1739");
            checkBoxCompatiLinefeedAs1739.Name = "checkBoxCompatiLinefeedAs1739";
            toolTip1.SetToolTip(checkBoxCompatiLinefeedAs1739, resources.GetString("checkBoxCompatiLinefeedAs1739.ToolTip"));
            checkBoxCompatiLinefeedAs1739.UseVisualStyleBackColor = true;
            // 
            // checkBoxCompatiRAND
            // 
            resources.ApplyResources(checkBoxCompatiRAND, "checkBoxCompatiRAND");
            checkBoxCompatiRAND.Name = "checkBoxCompatiRAND";
            toolTip1.SetToolTip(checkBoxCompatiRAND, resources.GetString("checkBoxCompatiRAND.ToolTip"));
            checkBoxCompatiRAND.UseVisualStyleBackColor = true;
            // 
            // label30
            // 
            resources.ApplyResources(label30, "label30");
            label30.Name = "label30";
            // 
            // checkBoxCompatiCALLNAME
            // 
            resources.ApplyResources(checkBoxCompatiCALLNAME, "checkBoxCompatiCALLNAME");
            checkBoxCompatiCALLNAME.Name = "checkBoxCompatiCALLNAME";
            toolTip1.SetToolTip(checkBoxCompatiCALLNAME, resources.GetString("checkBoxCompatiCALLNAME.ToolTip"));
            checkBoxCompatiCALLNAME.UseVisualStyleBackColor = true;
            // 
            // checkBoxCompatiErrorLine
            // 
            resources.ApplyResources(checkBoxCompatiErrorLine, "checkBoxCompatiErrorLine");
            checkBoxCompatiErrorLine.Name = "checkBoxCompatiErrorLine";
            toolTip1.SetToolTip(checkBoxCompatiErrorLine, resources.GetString("checkBoxCompatiErrorLine.ToolTip"));
            checkBoxCompatiErrorLine.UseVisualStyleBackColor = true;
            // 
            // tabPageDebug
            // 
            tabPageDebug.Controls.Add(button6);
            tabPageDebug.Controls.Add(button5);
            tabPageDebug.Controls.Add(checkBox23);
            tabPageDebug.Controls.Add(label15);
            tabPageDebug.Controls.Add(comboBox5);
            tabPageDebug.Controls.Add(label14);
            tabPageDebug.Controls.Add(comboBox4);
            tabPageDebug.Controls.Add(label13);
            tabPageDebug.Controls.Add(comboBox3);
            tabPageDebug.Controls.Add(label12);
            tabPageDebug.Controls.Add(comboBoxReduceArgumentOnLoad);
            tabPageDebug.Controls.Add(checkBox11);
            tabPageDebug.Controls.Add(checkBox13);
            resources.ApplyResources(tabPageDebug, "tabPageDebug");
            tabPageDebug.Name = "tabPageDebug";
            tabPageDebug.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            resources.ApplyResources(button6, "button6");
            button6.Name = "button6";
            button6.UseVisualStyleBackColor = true;
            button6.Click += button6_Click;
            // 
            // button5
            // 
            resources.ApplyResources(button5, "button5");
            button5.Name = "button5";
            button5.UseVisualStyleBackColor = true;
            button5.Click += button5_Click;
            // 
            // checkBox23
            // 
            resources.ApplyResources(checkBox23, "checkBox23");
            checkBox23.Name = "checkBox23";
            checkBox23.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            resources.ApplyResources(label15, "label15");
            label15.Name = "label15";
            // 
            // comboBox5
            // 
            comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            comboBox5.FormattingEnabled = true;
            comboBox5.Items.AddRange(new object[] { resources.GetString("comboBox5.Items"), resources.GetString("comboBox5.Items1"), resources.GetString("comboBox5.Items2"), resources.GetString("comboBox5.Items3") });
            resources.ApplyResources(comboBox5, "comboBox5");
            comboBox5.Name = "comboBox5";
            // 
            // label14
            // 
            resources.ApplyResources(label14, "label14");
            label14.Name = "label14";
            // 
            // comboBox4
            // 
            comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            comboBox4.FormattingEnabled = true;
            comboBox4.Items.AddRange(new object[] { resources.GetString("comboBox4.Items"), resources.GetString("comboBox4.Items1"), resources.GetString("comboBox4.Items2"), resources.GetString("comboBox4.Items3") });
            resources.ApplyResources(comboBox4, "comboBox4");
            comboBox4.Name = "comboBox4";
            // 
            // label13
            // 
            resources.ApplyResources(label13, "label13");
            label13.Name = "label13";
            // 
            // comboBox3
            // 
            comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            comboBox3.FormattingEnabled = true;
            comboBox3.Items.AddRange(new object[] { resources.GetString("comboBox3.Items"), resources.GetString("comboBox3.Items1"), resources.GetString("comboBox3.Items2"), resources.GetString("comboBox3.Items3") });
            resources.ApplyResources(comboBox3, "comboBox3");
            comboBox3.Name = "comboBox3";
            // 
            // label12
            // 
            resources.ApplyResources(label12, "label12");
            label12.Name = "label12";
            // 
            // comboBoxReduceArgumentOnLoad
            // 
            comboBoxReduceArgumentOnLoad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            comboBoxReduceArgumentOnLoad.FormattingEnabled = true;
            comboBoxReduceArgumentOnLoad.Items.AddRange(new object[] { resources.GetString("comboBoxReduceArgumentOnLoad.Items"), resources.GetString("comboBoxReduceArgumentOnLoad.Items1"), resources.GetString("comboBoxReduceArgumentOnLoad.Items2") });
            resources.ApplyResources(comboBoxReduceArgumentOnLoad, "comboBoxReduceArgumentOnLoad");
            comboBoxReduceArgumentOnLoad.Name = "comboBoxReduceArgumentOnLoad";
            comboBoxReduceArgumentOnLoad.SelectedIndexChanged += comboBoxReduceArgumentOnLoad_SelectedIndexChanged;
            // 
            // checkBox11
            // 
            resources.ApplyResources(checkBox11, "checkBox11");
            checkBox11.Name = "checkBox11";
            checkBox11.UseVisualStyleBackColor = true;
            // 
            // checkBox13
            // 
            resources.ApplyResources(checkBox13, "checkBox13");
            checkBox13.Name = "checkBox13";
            checkBox13.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            resources.ApplyResources(label16, "label16");
            label16.Name = "label16";
            // 
            // openFileDialog1
            // 
            openFileDialog1.FileName = "openFileDialog1";
            // 
            // ConfigDialog
            // 
            resources.ApplyResources(this, "$this");
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            Controls.Add(label16);
            Controls.Add(tabControl);
            Controls.Add(buttonReboot);
            Controls.Add(buttonCancel);
            Controls.Add(buttonSave);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "ConfigDialog";
            ShowIcon = false;
            ShowInTaskbar = false;
            Shown += shown;
            tabControl.ResumeLayout(false);
            tabEnvironment.ResumeLayout(false);
            tabEnvironment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)numericUpDown11).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown10).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown4).EndInit();
            tabPageView.ResumeLayout(false);
            tabPageView.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)numericUpDown9).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown1).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown7).EndInit();
            tabPageWindow.ResumeLayout(false);
            tabPageWindow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)numericUpDown8).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDownPosY).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDownPosX).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown3).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown2).EndInit();
            tabPageFont.ResumeLayout(false);
            flowLayoutPanel1.ResumeLayout(false);
            flowLayoutPanel1.PerformLayout();
            tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)numericUpDown5).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown6).EndInit();
            tabPageSystem.ResumeLayout(false);
            tabPageSystem.PerformLayout();
            tabPageSystem2.ResumeLayout(false);
            tabPageSystem2.PerformLayout();
            tabPageCompati.ResumeLayout(false);
            tabPageCompati.PerformLayout();
            tabPageDebug.ResumeLayout(false);
            tabPageDebug.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.Button buttonReboot;
		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.TabPage tabPageFont;
		private System.Windows.Forms.TabPage tabPageDebug;
		private MinorShift.Emuera.Forms.ColorBox colorBoxFG;
		private MinorShift.Emuera.Forms.ColorBox colorBoxBG;
		private MinorShift.Emuera.Forms.ColorBox colorBoxBacklog;
		private MinorShift.Emuera.Forms.ColorBox colorBoxSelecting;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.NumericUpDown numericUpDown6;
		private System.Windows.Forms.NumericUpDown numericUpDown5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox comboBox2;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.ComboBox comboBox4;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.ComboBox comboBox3;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.ComboBox comboBoxReduceArgumentOnLoad;
		private System.Windows.Forms.CheckBox checkBox11;
		private System.Windows.Forms.CheckBox checkBox13;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.ComboBox comboBox5;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TabPage tabPageView;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.NumericUpDown numericUpDown7;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown numericUpDown9;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.ComboBox comboBoxTextDrawingMode;
		private System.Windows.Forms.CheckBox checkBox14;
		private System.Windows.Forms.TabPage tabPageWindow;
		private System.Windows.Forms.CheckBox checkBox17;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.NumericUpDown numericUpDownPosY;
		private System.Windows.Forms.NumericUpDown numericUpDownPosX;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown numericUpDown3;
		private System.Windows.Forms.NumericUpDown numericUpDown2;
		private System.Windows.Forms.CheckBox checkBox8;
		private System.Windows.Forms.Label ScrollRange;
		private System.Windows.Forms.NumericUpDown numericUpDown8;
		private System.Windows.Forms.TabPage tabEnvironment;
		private System.Windows.Forms.NumericUpDown numericUpDown11;
		private System.Windows.Forms.CheckBox checkBox18;
		private System.Windows.Forms.NumericUpDown numericUpDown10;
		private System.Windows.Forms.NumericUpDown numericUpDown4;
		private System.Windows.Forms.CheckBox checkBox7;
		private System.Windows.Forms.CheckBox checkBox6;
		private System.Windows.Forms.CheckBox checkBox5;
		private System.Windows.Forms.CheckBox checkBox4;
		private System.Windows.Forms.CheckBox checkBox3;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.CheckBox checkBox21;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.TabPage tabPageCompati;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.CheckBox checkBoxCompatiCALLNAME;
		private System.Windows.Forms.CheckBox checkBoxCompatiErrorLine;
		private System.Windows.Forms.CheckBox checkBox24;
		private System.Windows.Forms.CheckBox checkBoxCompatiLinefeedAs1739;
		private System.Windows.Forms.CheckBox checkBoxCompatiRAND;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.CheckBox checkBox23;
		private System.Windows.Forms.CheckBox checkBoxFuncNoIgnoreCase;
		private System.Windows.Forms.TabPage tabPageSystem;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.CheckBox checkBoxSystemFullSpace;
		private System.Windows.Forms.CheckBox checkBox22;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.CheckBox checkBox20;
		private System.Windows.Forms.CheckBox checkBox19;
		private System.Windows.Forms.CheckBox checkBox16;
		private System.Windows.Forms.CheckBox checkBox15;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.CheckBox checkBox10;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.CheckBox checkBox25;
		private System.Windows.Forms.CheckBox checkBox12;
		private System.Windows.Forms.CheckBox checkBox28;
		private System.Windows.Forms.TabPage tabPageSystem2;
		private System.Windows.Forms.CheckBox checkBoxSystemTripleSymbol;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.CheckBox checkBox26;
		private System.Windows.Forms.CheckBox checkBox27;
		private System.Windows.Forms.CheckBox checkBoxCompatiSP;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.ComboBox comboBox6;
		private System.Windows.Forms.CheckBox checkBox9;
		private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox _useButtonFocusColor;
        private System.Windows.Forms.CheckBox _useNewRandom;
        private System.Windows.Forms.CheckBox _useScopedVariableInstruction;
    }
}